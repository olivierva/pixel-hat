#include <Adafruit_NeoPixel.h>
#include <Adafruit_NeoMatrix.h>
#include <gamma.h>
#include <Adafruit_GFX.h>

#ifndef MATIX_H_INCLUDE
#define MATIX_H_INCLUDE

// NEOPIXEL STUFF ----------------------------------------------------------

// 4 meters of NeoPixel strip is coiled around a top hat; the result is
// not a perfect grid.  My large-ish 61cm circumference hat accommodates
// 37 pixels around...a 240 pixel reel isn't quite enough for 7 rows all
// around, so there's 7 rows at the front, 6 at the back; a smaller hat
// will fare better.
#define NEO_PIN     6 // Arduino pin to NeoPixel data input
#define NEO_WIDTH  38 // Hat circumference in pixels
#define NEO_HEIGHT  7 // Number of pixel rows (round up if not equal)
#define NEO_OFFSET  (((NEO_WIDTH * NEO_HEIGHT) - 240) / 2)
#define FPS 20                                // Scrolling speed

// Pixel strip must be coiled counterclockwise, top to bottom, due to
// custom remap function (not a regular grid).
extern Adafruit_NeoMatrix matrix;

extern int           msgX;
extern unsigned long prevFrameTime;

uint16_t remapXY(uint16_t, uint16_t);
uint8_t unhex(char);
void scrollText(String);
void matrixSetup();
void setColor(String);

#endif