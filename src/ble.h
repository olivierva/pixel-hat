#include <Arduino.h>
#include <SPI.h>
#include <Adafruit_BLE.h>
#include <Adafruit_BluefruitLE_SPI.h>
#include <Adafruit_BluefruitLE_UART.h>
#include "BluefruitConfig.h"

#ifndef BLE_H_INCLUDED
#define BLE_H_INCLUDED

uint8_t readStr(char dest[], uint8_t maxlen);
void matrixSetup();
void bleSetup();

extern char          msg[];   
extern char          prevMsg[];        
extern uint8_t       msgLen;    
extern char          color[];        

extern Adafruit_BluefruitLE_SPI ble;

#endif
