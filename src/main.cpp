#include <Arduino.h>
/*--------------------------------------------------------------------------
  GUGGENHAT: a Bluefruit LE-enabled wearable NeoPixel marquee.

  Requires:
  - Arduino Micro or Leonardo microcontroller board.  An Arduino Uno will
    NOT work -- Bluetooth plus the large NeoPixel array requires the extra
    512 bytes available on the Micro/Leonardo boards.
  - Adafruit Bluefruit LE nRF8001 breakout: www.adafruit.com/products/1697
  - 4 Meters 60 NeoPixel LED strip: www.adafruit.com/product/1461
  - 3xAA alkaline cells, 4xAA NiMH or a beefy (e.g. 1200 mAh) LiPo battery.
  - Late-model Android or iOS phone or tablet running nRF UART or
    Bluefruit LE Connect app.
  - BLE_UART, NeoPixel, NeoMatrix and GFX libraries for Arduino.

  Written by Phil Burgess / Paint Your Dragon for Adafruit Industries.
  MIT license.  All text above must be included in any redistribution.
  --------------------------------------------------------------------------*/

#include "ble.h"
#include "matrix.h"

void setup() {
  strcpy(msg,"TREEE!!!");
  
  matrixSetup();
  bleSetup();

}

void loop() {
    ble.update(200);
  /*  
    if(msg[0] == '#') { // Color commands start with '#'
      strcpy(msg, color);
      strcpy(prevMsg, msg);
    } else {
      strcpy(msg, prevMsg);
    }
  */
    scrollText(msg);
}
