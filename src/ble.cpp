#include "ble.h"

#if SOFTWARE_SERIAL_AVAILABLE
  #include <SoftwareSerial.h>
#endif

char          msg[21]       = {0};            // BLE 20 char limit + NUL
char          color[21]     = {0};
char          prevMsg[21]   = {0};            
uint8_t       msgLen        = 0;              // Empty message

int32_t charid_string;
int32_t charid_number;

Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}

#define FACTORYRESET_ENABLE      1
#define MINIMUM_FIRMWARE_VERSION   "0.7.0"

#include "RGB.h"
#include "BluefruitConfig.h"

void connected(void)
{
  Serial.println("Connected");
  Serial.println( F("Connected") );
}

void BleUartRX(char data[], uint16_t len)
{
  Serial.write(data, len);
  strcpy(prevMsg, msg);
  strncpy(msg, data, len);
  msg[len] = 0;
  Serial.println();
}

// Read from BTLE into buffer, up to maxlen chars (remainder discarded).
// Does NOT append trailing NUL.  Returns number of bytes stored.
uint8_t readStr(char dest[], uint8_t maxlen) {
  uint8_t len = 0;
  while(ble.available()) {
    len = ble.readline();
    Serial.println("Received data");
    Serial.println(ble.buffer);
  }
  strncpy(msg, ble.buffer, len);
  //
  //ble.waitForOK();
  return len;
}

void bleSetup() {
  //while (!Serial);  // required for Flora & Micro
  //delay(500);
  strcpy(color, "#00FF00");

  Serial.begin(115200);
  Serial.println(F("Adafruit Bluefruit AT Command Example"));
  Serial.println(F("-------------------------------------"));

  /* Initialise the module */
  Serial.print(F("Initialising the Bluefruit LE module: "));

  if ( !ble.begin(VERBOSE_MODE) )
  {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  Serial.println( F("OK!") );

  if ( FACTORYRESET_ENABLE )
  {
    /* Perform a factory reset to make sure everything is in a known state */
    Serial.println(F("Performing a factory reset: "));
    if ( ! ble.factoryReset() ){
      error(F("Couldn't factory reset"));
    }
  }

  if ( !ble.isVersionAtLeast(MINIMUM_FIRMWARE_VERSION) )
  {
    error( F("Callback requires at least 0.7.0") );
  }

  ble.sendCommandCheckOK( F("AT+GATTADDSERVICE=uuid=0x1234") );
  ble.sendCommandWithIntReply( F("AT+GATTADDCHAR=UUID=0x2345,PROPERTIES=0x08,MIN_LEN=1,MAX_LEN=6,DATATYPE=string,DESCRIPTION=string,VALUE=abc"), &charid_string);
  ble.sendCommandWithIntReply( F("AT+GATTADDCHAR=UUID=0x6789,PROPERTIES=0x08,MIN_LEN=4,MAX_LEN=4,DATATYPE=INTEGER,DESCRIPTION=number,VALUE=0"), &charid_number);

  ble.reset();
  ble.setMode(BLUEFRUIT_MODE_DATA);
  
  /* Disable command echo from Bluefruit */
  ble.echo(false);

  Serial.println("Requesting Bluefruit info:");
  /* Print Bluefruit information */
  ble.info();  
  ble.setConnectCallback(connected);
  ble.setBleUartRxCallback(BleUartRX);
  
}