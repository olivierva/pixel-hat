#include "matrix.h"

// Pixel strip must be coiled counterclockwise, top to bottom, due to
// custom remap function (not a regular grid).
Adafruit_NeoMatrix matrix(NEO_WIDTH, NEO_HEIGHT, NEO_PIN,
  NEO_MATRIX_TOP  + NEO_MATRIX_LEFT +
  NEO_MATRIX_ROWS + NEO_MATRIX_PROGRESSIVE,
  NEO_GRB         + NEO_KHZ800);

int           msgX          = matrix.width(); // Start off right edge
unsigned long prevFrameTime = 0L;             // For animation timing


// UTILITY FUNCTIONS -------------------------------------------------------

// Because the NeoPixel strip is coiled and not a uniform grid, a special
// remapping function is used for the NeoMatrix library.  Given an X and Y
// grid position, this returns the corresponding strip pixel number.
// Any off-strip pixels are automatically clipped by the NeoPixel library.
uint16_t remapXY(uint16_t x, uint16_t y) {
  return y * NEO_WIDTH + x - NEO_OFFSET;
}

// Given hexadecimal character [0-9,a-f], return decimal value (0 if invalid)
uint8_t unhex(char c) {
  return ((c >= '0') && (c <= '9')) ?      c - '0' :
         ((c >= 'a') && (c <= 'f')) ? 10 + c - 'a' :
         ((c >= 'A') && (c <= 'F')) ? 10 + c - 'A' : 0;
}

void scrollText(String textToDisplay) {
  int x = matrix.width();

  // Account for 6 pixel wide characters plus a space
  int pixelsInText = textToDisplay.length() * 7 + 35;

  
  matrix.setCursor(x, 0);
  matrix.print(textToDisplay);
  matrix.show();

  while(x > (matrix.width() - pixelsInText)) {
    //matrix.fillScreen(matrix.Color(red.r, red.g, red.b));
    matrix.fillScreen(0);
    matrix.setCursor(--x, 0);
    matrix.print(textToDisplay);
    matrix.show();
    delay(150);
  }
}

void matrixSetup() {
    matrix.begin();
    matrix.setRemapFunction(remapXY);
    matrix.setTextWrap(false);   // Allow scrolling off left
    matrix.setTextColor(0xF800); // Red by default
    matrix.setBrightness(31);    // Batteries have limited sauce
    matrix.fillScreen(0);
}

void setColor(String color) {
    Serial.println(color);
    switch(sizeof(color)) {
        case 4:                  // #RGB    4/4/4 RGB
        matrix.setTextColor(matrix.Color(
            unhex(color[1]) * 17, // Expand to 8/8/8
            unhex(color[2]) * 17,
            unhex(color[3]) * 17));
        break;
        case 5:                  // #XXXX   5/6/5 RGB
        matrix.setTextColor(
            (unhex(color[1]) << 12) +
            (unhex(color[2]) <<  8) +
            (unhex(color[3]) <<  4) +
            unhex(color[4]));
        break;
        case 7:                  // #RRGGBB 8/8/8 RGB
        matrix.setTextColor(matrix.Color(
            (unhex(color[1]) << 4) + unhex(color[2]),
            (unhex(color[3]) << 4) + unhex(color[4]),
            (unhex(color[5]) << 4) + unhex(color[6])));
        break;
    }
}
